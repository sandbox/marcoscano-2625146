<?php
/**
 * Variables available:
 *
 * $image - Contains the image html rendered by Drupal.
 * $width - Image width in px.
 * $height - Image height in px.
 * $color_value - Color value selected by the user.
 * $opacity_value - Opacity value selected by the user.
 *
 */
?>
<div class="image-opacity-overlay mode-overlay"
  <?php if ($rgba): ?>
    data-rgba="<?php print $rgba; ?>">
  <div class="overlay" style="background-color: <?php print $rgba; ?>; height: <?php print $height; ?>px; width: <?php print $width; ?>px;"></div>
  <?php else: ?>
    >
  <?php endif;?>
  <div class="img">
    <?php print $image; ?>
  </div>
</div>
