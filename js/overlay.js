(function ($) {
  Drupal.behaviors.imageOpacityOverlay = {
    attach: function (context, settings) {

    // Make sure the position of the overlay matches the position of the image.
    $('.image-opacity-overlay.mode-overlay .overlay').each(function() {
      var $overlay = $(this);
      var $image = $overlay.next('img');
      var overlay_offset = $overlay.offset();
      var image_offset = $image.offset();
      if (overlay_offset.top != image_offset.top
        || overlay_offset.left != image_offset.left) {

        // Place the overlay on the top of the image.
        $overlay.offset(image_offset);

      }
    });

    }
  };
})(jQuery);
