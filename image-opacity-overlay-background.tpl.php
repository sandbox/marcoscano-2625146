<?php
/**
 * Variables available:
 *
 * $image - Contains the image html rendered by Drupal.
 * $width - Image width in px.
 * $height - Image height in px.
 * $color_value - Color value selected by the user.
 * $opacity_value - Opacity value selected by the user.
 *
 */
?>
<div class="image-opacity-overlay mode-background"
  <?php if ($rgba): ?>
    data-rgba="<?php print $rgba; ?>"
    style="background-color:<?php print $rgba; ?>;"
  <?php endif;?>>
  <div class="img">
    <?php print $image; ?>
  </div>
</div>
